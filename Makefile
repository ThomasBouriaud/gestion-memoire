java:
	@javac Main.java
	@java Main
	@rm *.class

cpp-simple: --private-cpp-simple run
--private-cpp-simple:
	$(eval MAIN := simple)

cpp-referenced: --private-cpp-referenced run
--private-cpp-referenced:
	$(eval MAIN := referenced)

cpp-deep-copy: --private-deep-copy run
--private-deep-copy:
	$(eval MAIN := deep-copy)

cpp-pointer-value: --privatee-cpp-pointer-value run
--privatee-cpp-pointer-value:
	$(eval MAIN := pointer-value)

cpp-pointer-reference: --privatee-cpp-pointer-reference run
--privatee-cpp-pointer-reference:
	$(eval MAIN := pointer-reference)

run:
	@g++ Main.$(MAIN).cpp
	@./a.out
	@rm a.out

install:
	npm install -g reveal-md

slides:
	reveal-md diapo.md --vertical-sep
