public class Main {

    public static void updateField(A a2) {
        a2.message = "Reference";
    }

    public static void reassignReference(A a3) {
        a3 = new A();
        a3.message = "Reference";
    }

    public static void main(String[] args) {
        A a1 = new A();
        a1.message = "Valeur";

        updateField(a1);
        System.out.println(a1.message);

        a1.message = "Valeur";
        reassignReference(a1);
        System.out.println(a1.message);
    }
}

class A {
    String message = "";
}