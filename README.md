# Gestion mémoire

## CPP

g++ --version : **gcc version 11.3.0 (Ubuntu 11.3.0-1ubuntu1~22.04)**

### Simple
```shell
make cpp-simple
```

### Using referencs
```shell
make cpp-referenced
```

### Using pointer
```shell
make cpp-pointer
```

### Deep copy
```shell
make cpp-deep-copy 
```

## Java
java -version : **java version "17.0.6" 2023-01-17 LTS**
```shell
make java
```

## Display slides
### Installation de reveal.js
```shell
make install
```

### Lancement des slides
```shell
make slides
```