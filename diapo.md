# Gestion de la mémoire

```text
class A
    string message = ""

updateField(A a2)
    a2.message = "Reference"

reassignReference(A a3)
    a3 = new A()
    a3.message = "Reference"

main()
    A a1 = new A()
    a1.message = "Valeur"

    updateField(a1)
    afficher(a1.message)

    a1.message = "Valeur"
    reassignReference(a1)
    afficher(a1.message)
```

---

# Gestion de la mémoire en CPP

- Utilisation standard
- Passage par référence
- Utilisation des pointeurs

---

# CPP Simple

```cpp
void updateField(A a2) {
	a2.message = "Reference";
}

void reassignReference(A a2) {
	a2 = A();
	a2.message = "Reference";
}

int main() {
	A a1;

	a1.message = "Valeur";
	updateField(a1);
	std::cout << a1.message << "\n";

	a1.message = "Valeur";
	reassignReference(a1);
	std::cout << a1.message << "\n";

	return 0;
}
```

Qu’est ce qui est affiché ?

```text
Valeur
Valeur
```

---

# CPP Simple

TODO explication de la stack

---

# CPP Reference

```cpp
void updateField(A & a2) {
	a2.message = "Reference";
}

void reassignReference(A & a2) {
	a2 = A();
	a2.message = "Reference";
}

int main() {
	A a1;

	a1.message = "Valeur";
	updateField(a1);
	std::cout << a1.message << "\n";

	a1.message = "Valeur";
	reassignReference(a1);
	std::cout << a1.message << "\n";

	return 0;
}
```

Qu’est ce qui est affiché ?

```text
Reference
Reference
```

---

# CPP Reference

TODO explication de la stack

---

# CPP Pointer

```cpp
void updateField(A * a2) {
	a2->message = "Reference";
}

void reassignReference(A * a2) {
	A a3 = A();
	a3.message = "Reference";
	a2 = & a3;
}

int main() {
	A obj;
	A * a1 = & obj;

	a1->message = "Valeur";
	updateField(a1);
	std::cout << a1->message << "\n";

	a1->message = "Valeur";
	reassignReference(a1);
	std::cout << a1->message << "\n";

	return 0;
}
```

Qu’est ce qui est affiché ?

```text
Reference
Valeur
```

---

# CPP Pointer

TODO explication de la stack

---

# Java

```java
public class Main {

    public static void updateField(A a2) {
        a2.message = "Reference";
    }

    public static void reassignReference(A a3) {
        a3 = new A();
        a3.message = "Reference";
    }

    public static void main(String[] args) {
        A a1 = new A();
        a1.message = "Valeur";

        updateField(a1);
        System.out.println(a1.message);

        a1.message = "Valeur";
        reassignReference(a1);
        System.out.println(a1.message);
    }
}
```

Qu’est ce qui est affiché ?

```text
Reference
Valeur
```
