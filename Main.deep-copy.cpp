#include <iostream>

class B {
	public:
		int value = 0;
};

class A {
	public:
		int value = 0;
		B nested;
};

int main() {
	A a1;
	a1.value = 1;
	a1.nested.value = 2;

	A a2 = a1;
	std::cout << "Adresse de a1 : " << & a1 << "\n";
	std::cout << "Adresse de a2 : " << & a2 << "\n";

	std::cout << "Adresse de a1.nested : " << & a1.nested << "\n";
	std::cout << "Adresse de a2.nested : " << & a2.nested << "\n";

	std::cout << "Updating values \n";
	a2.value = 10;
	a2.nested.value = 11;

	std::cout << "A1 value : " << a1.value << "\n";
	std::cout << "A1 nested  : " << a1.nested.value << "\n";
	std::cout << "A2 value : " << a2.value << "\n";
	std::cout << "A2 nested  : " << a2.nested.value << "\n";

	return 0;
}
