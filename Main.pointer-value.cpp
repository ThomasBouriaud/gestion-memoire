#include <iostream>

class A {
	public:
		std::string message = "";
};

void updateField(A * a2) {
	a2->message = "Reference";
}

void reassignReference(A * a3) {
	A a4 = A();
	a3 = & a4;
	a3->message = "Reference";
}

int main() {
	A obj;
	A * a1 = & obj;

	a1->message = "Valeur";
	updateField(a1);
	std::cout << a1->message << "\n";

	a1->message = "Valeur";
	reassignReference(a1);
	std::cout << a1->message << "\n";

	return 0;
}
